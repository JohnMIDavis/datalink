/*
-------------------
DESCRIPTION
-------------------

Created By: David Mielcarek, Lower Columbia College, 20220301
Modified By: David Mielcarek, Lower Columbia College, 20220310

This is the standards for creation a query file.

The following standards have been defined:

* A query file should be placed in the directory for the usage.  If a new category is needed, please update the "directoryStructure" file at the root of this repository.

* A query file name should use Syntax: [PLATFORM]-[NAME].txt.  The PLATFORM would be like 'PLSQL', 'TSQL', etc.  The NAME would be an short identifier of the purpose.

* A query file should be able to be used as-is in a query window (except any filters to change).

* Any non-SQL-code should be enclosed in remark format.

* The following sections should be included, in this order:
    * DESCRIPTION: should include the Created By/Modified By, and a description of the purpose.
    * FILTERED LINES: any lines that can be modified to control the query.
    * CODE: the SQL code.

Any sections that are not needed should still be included, but have n/a.

-------------------
FILTERED LINES
-------------------

This seciton will detail any filters that can be modified to control the query.  A copy of the line(s) and a note about each.

Example:
Line: AND [T4_1].[STRM] IN ('2211','2213','2215','2217')
Note: Change to the Terms you want to scope.

-------------------
CODE
-------------------

This section contains the SQL code that will run.
*/

SELECT 
[T1].[EMPLID]
,[T1].[LAST_NAME]
,[T1].[FIRST_NAME]
,[T1].[MIDDLE_NAME]
,[T1].[BIRTHDATE]
,[T4].[STRM]
,[T3].[ACAD_PLAN]
FROM [SYSADM_CS].[PS_PERSONAL_DATA_GGVW] [T1]

OUTER APPLY (
SELECT
[T3_1].[EMPLID],[T3_1].[EFFDT],[T3_1].[EFFSEQ],[T3_1].[ACAD_PLAN],[T3_1].[REQ_TERM]
FROM [SYSADM_CS].[PS_ACAD_PLAN_GGVW] [T3_1]
WHERE
(
  [T3_1].[REQ_TERM] = 
  (SELECT MAX([T3_2].[REQ_TERM]) FROM SYSADM_CS.PS_ACAD_PLAN_GGVW [T3_2]
   WHERE [T3_2].[EMPLID] = [T3_1].[EMPLID]
  )
  AND
  [T3_1].[EFFDT] = 
  (SELECT MAX([T3_2].[EFFDT]) FROM SYSADM_CS.PS_ACAD_PLAN_GGVW [T3_2]
   WHERE [T3_2].[EMPLID] = [T3_1].[EMPLID]
   AND [T3_2].[REQ_TERM] = [T3_1].[REQ_TERM]
  )
  AND [T3_1].[EFFSEQ] = 
  (SELECT MAX([T3_2].[EFFSEQ]) FROM SYSADM_CS.PS_ACAD_PLAN_GGVW [T3_2]
   WHERE [T3_2].[EMPLID] = [T3_1].[EMPLID]
   AND [T3_2].[REQ_TERM] = [T3_1].[REQ_TERM]
   AND [T3_2].[EFFDT] = [T3_1].[EFFDT]
  )
  AND [T3_1].[STDNT_CAR_NBR] = 
  (SELECT MAX([T3_2].[STDNT_CAR_NBR]) FROM SYSADM_CS.PS_ACAD_PLAN_GGVW [T3_2]
   WHERE [T3_2].[EMPLID] = [T3_1].[EMPLID]
   AND [T3_2].[REQ_TERM] = [T3_1].[REQ_TERM]
   AND [T3_2].[EFFDT] = [T3_1].[EFFDT]
   AND [T3_2].[EFFSEQ] = [T3_1].[EFFSEQ]
  )
  AND [T3_1].[ACAD_PLAN] = 
  (SELECT MAX([T3_2].[ACAD_PLAN]) FROM SYSADM_CS.PS_ACAD_PLAN_GGVW [T3_2]
   WHERE [T3_2].[EMPLID] = [T3_1].[EMPLID]
   AND [T3_2].[REQ_TERM] = [T3_1].[REQ_TERM]
   AND [T3_2].[EFFDT] = [T3_1].[EFFDT]
   AND [T3_2].[EFFSEQ] = [T3_1].[EFFSEQ]
  )
)
AND [T3_1].[EMPLID]=[T1].[EMPLID]
) [T3]

CROSS APPLY (
SELECT
[T4_1].[EMPLID],[T4_1].[STRM],[T4_1].[ACAD_CAREER],[T4_1].[STDNT_CAR_NBR],[T4_1].[ACAD_PROG_PRIMARY]
FROM [SYSADM_CS].[PS_STDNT_CAR_TERM] [T4_1]
WHERE 
(
[T4_1].[STRM]=
(
SELECT MAX([T4_2].[STRM])
FROM [SYSADM_CS].[PS_STDNT_CAR_TERM] [T4_2]
WHERE [T4_2].[EMPLID]=[T4_1].[EMPLID]
)
AND [T4_1].[STDNT_CAR_NBR]=
(
SELECT MAX([T4_2].[STDNT_CAR_NBR])
FROM [SYSADM_CS].[PS_STDNT_CAR_TERM] [T4_2]
WHERE [T4_2].[EMPLID]=[T4_1].[EMPLID]
AND [T4_2].[STRM]=[T4_1].[STRM]
)
)
AND [T4_1].[STRM] IN ('2211','2213','2215','2217')
AND [T4_1].[ACAD_CAREER]='UGRD'
AND [T4_1].[EMPLID]=[T1].[EMPLID]
) [T4]

ORDER BY [T1].[EMPLID]
;
