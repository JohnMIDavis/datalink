/*
-------------------
DESCRIPTION
-------------------

Created By: David Mielcarek, Lower Columbia College, 20211116
Modified By: David Mielcarek, Lower Columbia College, 20211116

Gather active student information, where active term is -30 days and forward, equals 'UGRD', Academic Load != 'N', at least 1 unit taken, and not withdrawn.

-------------------
FILTERED LINES
-------------------
Line: SET @lccDTDateAdd=DATEADD(DAY,-30,GETDATE());
Note: Change "-30" to how far back to check.

-------------------
CODE
-------------------
*/

DECLARE @lccDTDateAdd DateTime;
SET @lccDTDateAdd=DATEADD(DAY,-30,GETDATE());
SELECT
[T1_1].[EMPLID]
,[T1_1].[STRM]
,[T1_1].[ACAD_CAREER]
,[T1_1].[STDNT_CAR_NBR]
,[T1_1].[ACAD_PROG_PRIMARY]
,[T1_1].[CUM_GPA]
,[T1_1].[TOT_CUMULATIVE]
,[T1_1].[UNT_TAKEN_PRGRSS]
,[T1_1].[ACADEMIC_LOAD]
,[T1_1].[UNT_AUDIT]
FROM [SYSADM_CS].[PS_STDNT_CAR_TERM] [T1_1]
LEFT JOIN [SYSADM_CS].[PS_TERM_TBL] [T1_2] ON [T1_2].[STRM]=[T1_1].[STRM] AND [T1_2].[ACAD_CAREER]=[T1_1].[ACAD_CAREER]
WHERE 
(
[T1_1].[STRM]=
(
SELECT MAX([T1_2].[STRM])
FROM [SYSADM_CS].[PS_STDNT_CAR_TERM] [T1_2]
WHERE [T1_2].[EMPLID]=[T1_1].[EMPLID]
)
AND [T1_1].[STDNT_CAR_NBR]=
(
SELECT MAX([T1_2].[STDNT_CAR_NBR])
FROM [SYSADM_CS].[PS_STDNT_CAR_TERM] [T1_2]
WHERE [T1_2].[EMPLID]=[T1_1].[EMPLID]
AND [T1_2].[STRM]=[T1_1].[STRM]
)
)
AND
 [T1_2].[TERM_END_DT]>=@lccDTDateAdd
AND [T1_1].[ACAD_CAREER]='UGRD'
AND ([T1_1].[UNT_TAKEN_PRGRSS]>0 OR [T1_1].[UNT_AUDIT]>0)
AND [T1_1].[WITHDRAW_DATE] IS null
ORDER BY STRM,EMPLID
;