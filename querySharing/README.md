# README #

This README is to describe this Query Sharing repository.

### What Is This Repository For? ###

* Sharing Queries/SQL Code
* Setting Standards

### Retrieving Queries Guidelines ###

* Look At The directoryStructure.txt To Understand How Queries Are Categorized
* Go To The Desired Directory
* Download/Use The Query
* Each Query Will Have A Description, Filters Used, etc. At Top

### Contribution Guidelines ###

* Read/Follow The queryFileTemplate.txt Standards
* Place A New Query Into The Correct Category, If Non Match, Create One
* If Creating A New Category, Please Update The directoryStructure.txt 

### Contact Resource ###

* David Mielcarek, Lower Columbia College
* (hopefully) ...future others...